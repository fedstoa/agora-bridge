# Agora Bridge

A connector Agora <-> social networks.

Will initially support [[mastodon]] and [[twitter]].

See https://anagora.org/node/an-agora for more.
